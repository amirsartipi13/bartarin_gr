#ifndef _snake_H_
#define _snake_H_
#include <Windows.h>
#include <conio.h>
#include <iostream>
#include<stdlib.h>
#include<time.h>
#include"node.h"
using namespace std;
class snake
{
public:
    int i, j, e, m;
    char dir,lastdir;
    node *head, *tail, *t;
    snake();
    void adddata(int o, int p);
    void move(int x) ;
    int food_state_i(int x);
    int food_state_j(int x);
};
#endif
