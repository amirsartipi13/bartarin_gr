#ifndef _board_H_
#define _board_H_
#include"Snake.h"
#include"food.h"
class board
{
public:
    int add;
    snake s;
    node s1;
    food s2;
    node *t;
    int updated;
    int i1, j1,irand,jrand;
    int score=0, chap;
    snake *getsnake();
    node *getnode();
    food *getfood();
    void print(int x);
    void update(int x);
    void run();
    board operator ++(int);
};
#endif
