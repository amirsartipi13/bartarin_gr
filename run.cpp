#include"Snake.h"
#include"board.h"
#include"snake_game.h"
void snake_game::run()
{
    cout << "~~~~~~~~~~~~~~~~~~~~~" << endl;
    cout << "~                   ~" << endl;
    cout << "~   SNAKE GAME      ~" << endl;
    cout << "~                   ~" << endl;
    cout << "~   1.Play          ~" << endl;
    cout << "~                   ~" << endl;
    cout << "~   2.Exit          ~" << endl;
    cout << "~                   ~" << endl;
    cout << "~~~~~~~~~~~~~~~~~~~~~" << endl;
    int anser;
    cin >> anser;
    if (anser == 2)
    {
        exit(0);
    }
    if (anser == 1)
    {
        int x;
        int i = 100;
        srand(time(0));
        cin >> x;
        getboard()->getsnake()->e = 0;
        getboard()->i1 = getboard()->getsnake()->food_state_i(x);
        getboard()->j1 = getboard()->getsnake()->food_state_j(x);
        getboard()->getfood()->savefood(getboard()->i1,getboard()->j1);
        getboard()->irand=getboard()->i1;
        getboard()->jrand=getboard()->j1;
        while (true)
        {
            if (getboard()->getsnake()->e > 0)
                break;
            if (_kbhit())
                getboard()->getsnake()->dir=_getch();
            getboard()->update(x);
            getboard()->print(x);
            if (getboard()->score % 40 == 0 && getboard()->score != 0 && i != 25 && getboard()->updated == 1)
                i = i - 25;
            getboard()->updated = 0;
            Sleep(i);
            system("cls");
        }
        cout << "~~~~~~~~~~~~~~~~~~~~~" << endl;
        cout << "~                   ~" << endl;
        cout << "~     GAME OVER     ~" << endl;
        cout << "~                   ~" << endl;
        cout << "~~~~~~~~~~~~~~~~~~~~~" << endl;
        cout << " SCORE : " << getboard()->score << endl;
    }
}

