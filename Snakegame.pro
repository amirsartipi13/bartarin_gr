QT += core
QT -= gui

TARGET = Snakegame
CONFIG += console
CONFIG -= app_bundle
CONFIG  +=c++11
TEMPLATE = app

SOURCES += main.cpp \
    boardprint.cpp \
    snakeadddata.cpp \
    snakectr.cpp \
    snakemove.cpp \
    foodgeneratori.cpp \
    foodgeneratorj.cpp \
    boardupdate.cpp \
    getsnake.cpp \
    getnode.cpp \
    savefood.cpp \
    getfood.cpp \
    run.cpp \
    getboard.cpp \
    boardoveload.cpp
HEADERS += Snake.h \
    board.h \
    node.h \
    food.h \
    snake_game.h

OTHER_FILES +=

